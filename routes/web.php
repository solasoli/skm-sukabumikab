<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\LaporanController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'DashboardController@index')->name('home');

/* Login section */
Route::get('/login', 'Auth\CustomLoginController@index')->name('login');
Route::get('/403', 'Auth\CustomLoginController@not_allowed')->name('unauthorized');
Route::post('/login', 'Auth\CustomLoginController@login_proses');
Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('survey')->group(function () {
  Route::get('/{slug}', 'Srv\SurveyController@create');
  Route::get('/terimakasih/{id}', 'Srv\SurveyController@edit');
  Route::get('/share/{id}', 'Srv\SurveyController@share');
  Route::get('/preview/{slug}', 'Srv\SurveyController@preview');
  /* Post section */
  Route::post('/{slug}', 'Srv\SurveyController@store');
  Route::post('/terimakasih/{id}', 'Srv\SurveyController@update');
});


Route::middleware(['auth'])->group(function () {

  Route::get('/dashboard/tentang', 'DashboardController@tentang');
  Route::get('/dashboard/skema', 'DashboardController@skema');

  Route::get('/sync_menu', 'KonfigurasiController@sync_menu');

  Route::prefix('acl')->middleware('auth.super_admin')->group(function () {

    Route::prefix('role')->group(function () {
      Route::get('/', 'ACL\RoleController@index');
      Route::get('/add', 'ACL\RoleController@create');
      Route::get('/edit/{id}', 'ACL\RoleController@edit');
      Route::get('/delete/{id}', 'ACL\RoleController@destroy');
      Route::get('/datatables', 'ACL\RoleController@list_datatables_api');
      /* Post section */
      Route::post('/add', 'ACL\RoleController@store');
      Route::post('/edit/{id}', 'ACL\RoleController@update');
    });

    Route::prefix('menu')->group(function () {
      Route::get('/', 'ACL\MenuController@index');
      Route::get('/add', 'ACL\MenuController@create');
      Route::get('/edit/{id}', 'ACL\MenuController@edit');
      Route::get('/delete/{id}', 'ACL\MenuController@destroy');
      Route::get('/datatables', 'ACL\MenuController@list_datatables_api');
      /* Post section */
      Route::post('/add', 'ACL\MenuController@store');
      Route::post('/edit/{id}', 'ACL\MenuController@update');
    });

    Route::prefix('permission')->group(function () {
      Route::get('/{role?}', 'ACL\PermissionController@index');
      Route::post('/{role?}', 'ACL\PermissionController@store');
    });


    Route::prefix('user')->group(function () {
      Route::get('/', 'ACL\UserController@index');
      Route::get('/add', 'ACL\UserController@create');
      Route::get('/edit/{id}', 'ACL\UserController@edit');
      Route::get('/delete/{id}', 'ACL\UserController@destroy');
      Route::get('/datatables', 'ACL\UserController@list_datatables_api');
      /* Post section */
      Route::post('/add', 'ACL\UserController@store');
      Route::post('/edit/{id}', 'ACL\UserController@update');
    });

  });

  Route::prefix('srv')->group(function () {
    Route::prefix('penerbitan')->group(function () {
      Route::get('/', 'Srv\GrupPertanyaanController@penerbitan');
      Route::get('/publish/{slug}/{status}', 'Srv\GrupPertanyaanController@publish');
      Route::get('/datatables/{status}', 'Srv\GrupPertanyaanController@penerbitan_datatables_api');
    });

    Route::prefix('grup_pertanyaan')->group(function () {
      Route::get('/', 'Srv\GrupPertanyaanController@index');
      Route::get('/add', 'Srv\GrupPertanyaanController@create');
      Route::get('/edit/{id}', 'Srv\GrupPertanyaanController@edit');
      Route::get('/delete/{id}', 'Srv\GrupPertanyaanController@destroy');
      Route::get('/datatables', 'Srv\GrupPertanyaanController@list_datatables_api');
      Route::post('/get_pertanyaan_select2/{guid_tipe_pertanyaan}', 'Srv\GrupPertanyaanController@get_pertanyaan_select2');
      Route::post('/get_pertanyaan_random/{guid_tipe_pertanyaan}', 'Srv\GrupPertanyaanController@get_pertanyaan_random');
      /* Post section */
      Route::post('/add', 'Srv\GrupPertanyaanController@store');
      Route::post('/edit/{id}', 'Srv\GrupPertanyaanController@update');
      Route::post('/get_grup_by_id_skpd', 'Srv\GrupPertanyaanController@get_grup_by_id_skpd');
    });

    Route::prefix('pertanyaan')->group(function () {
      Route::get('/', 'Srv\PertanyaanController@index');
      Route::get('/add', 'Srv\PertanyaanController@create');
      Route::get('/edit/{id}', 'Srv\PertanyaanController@edit');
      Route::get('/delete/{id}', 'Srv\PertanyaanController@destroy');
      Route::get('/datatables', 'Srv\PertanyaanController@list_datatables_api');
      Route::get('/get_pertanyaan_by_id/{id}', 'Srv\PertanyaanController@get_pertanyaan_by_id');
      /* Post section */
      Route::post('/add', 'Srv\PertanyaanController@store');
      Route::post('/edit/{id}', 'Srv\PertanyaanController@update');
    });
  });

  /* Laporan */
  Route::prefix('laporan')->name('laporan.')->group(function () {
    Route::get('/srv_tahunan', 'LaporanController@srv_tahunan');
    Route::post('/srv_tahunan', 'LaporanController@srv_tahunan');

    Route::get('/srv_bulanan', 'LaporanController@srv_bulanan');
    Route::get('/srv_bulanan/get_tahun_select2/{id_skpd}', 'LaporanController@get_tahun_skpd')->middleware('auth.skpd');
    Route::get('/srv_bulanan/get_pensurvey/{id_grup}', 'LaporanController@get_pensurvey');
    Route::post('/srv_bulanan', 'LaporanController@srv_bulanan');

    Route::get('/print_responden_bulanan', 'LaporanController@print_responden_bulanan');
    Route::post('/print_responden_bulanan', 'LaporanController@print_responden_bulanan');

    Route::prefix('lapor_ikm')->group(function() {
      Route::get('/', 'LaporanController@lapor_ikm');
      Route::post('/', 'LaporanController@lapor_ikm');
      Route::get('/get', 'LaporanController@lapor_ikm_get');
      Route::get('/get_tahun/{id_skpd}', 'LaporanController@get_tahun_skpd')->middleware('auth.skpd');
      Route::get('/get_data_survey/{id_survey}/{tahun}/{opd}', 'LaporanController@get_data_survey');
      Route::get('/get_data_seluruh_survey/{tahun}/{opd}', 'LaporanController@get_seluruh_data_survey');
    });

    // Laporan IKM
    Route::get('/ikm', 'LaporanController@ikm');

    // Laporan IKM tingkat kota
    Route::get('/ikm_tingkat_kota', 'LaporanController@ikm_tingkat_kota')->name('ikm_tingkat_kota');

    Route::get('/opds/{idOpd}/survey')->uses('LaporanController@opdSurveys')->name('laporan.opds.survey');

    Route::get('/unsur_pelayanan_survey')->uses('LaporanController@unsurPelayananSurvey');
    Route::get('/unsur_tertimbang')->uses('LaporanController@unsurTertimbang');
    Route::get('/unsur_tertimbang/{tahun}')->uses('LaporanController@getScoreTertimbang');

    Route::get('/unsur_pelayanan_survey_pertahun')->uses('LaporanController@unsurPelayananSurveyPerTahun');
    Route::get('/unsur_pelayanan_perbulan')->uses('LaporanController@unsurPelayananSurveyPerBulan');
    Route::get('/rekapitulasi_nilai_skm')->uses('LaporanController@rekapitulasiNilaiSkm');
  });

  /*
  * Konfigurasi
  */

  Route::prefix('konfigurasi')->group(function () {
      Route::get('/tentang', 'KonfigurasiController@tentang');
      Route::post('/tentang', 'KonfigurasiController@store_tentang');

      Route::get('/skema', 'KonfigurasiController@skema');
      Route::post('/skema', 'KonfigurasiController@store_skema');

      Route::get('/default_jawaban', 'KonfigurasiController@default_jawaban');
      Route::post('/default_jawaban', 'KonfigurasiController@store_default_jawaban');


      Route::get('/get_default_jawaban/{guid}', 'KonfigurasiController@get_default_jawaban');
  });

  Route::prefix('master')->group(function () {
    Route::get('/skpd')->uses('SkpdController@index');
    Route::get('/skpd/getSkpd')->uses('SkpdController@getSkpd');
    Route::post('/skpd/add')->uses('SkpdController@store');
    Route::get('/skpd/delete/{id}', 'SkpdController@destroy');
    Route::get('/skpd/edit/{id}', 'SkpdController@edit');
    Route::post('/skpd/update/{id}', 'SkpdController@update');
  });
  
  Route::get('/skpds/{year}/without-survey')->name('skpds.without_survey')->uses('SkpdController@skpdWithoutSurvey');
  Route::get('/respondents')->uses('RespondentController@dataTable')->name('respondents.datatable');

});

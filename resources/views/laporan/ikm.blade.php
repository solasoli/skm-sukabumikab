@php
    /** @var string $selectedSkpd*/
    /** @var string $selectedYear*/
    /** @var array $skpd*/
@endphp

@extends('layouts.app')

@section('content')
    <style>
        i.fa {
            font-size: 5em;
        }
    </style>
    <div class="page-title">
        <div class="title_left">
            <h3>Laporan IKM</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="list-unstyled">
                @foreach ($errors->all() as $error)
                    <li>&#8226; {{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <p class="alert alert-success">{!! Session::get('message') !!}</p>
    @endif

    <form class="form-horizontal" method="GET">
        @include('laporan.partials.opd_options',[
                      'skpd' => $skpd,
                      'selectedSkpd' => $selectedSkpd,
                      'title' => 'Filter Laporan Per OPD'
                      ])
    </form>

    @if($selectedSkpd)
        @include('laporan.partials.opd_chart_yearly', compact('ikm_tahunan_asc', 'ikm_tahunan_desc', 'selectedYear'))
    @endif

@endsection

@extends('layouts.app')

@section('content')
    <div class="page-title">
        <div class="text-center">
            <h3>Unsur Pelayanan Survey</h3>
        </div>
    </div>

    <!--<div class="clearfix"></div>-->

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="list-unstyled">
                @foreach ($errors->all() as $error)
                    <li>&#8226; {{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <p class="alert alert-success">{!! Session::get('message') !!}</p>
    @endif

    <form class="form-horizontal" method="GET" id="form_filter" style="margin-top: 50px;">
        @include('laporan.partials.opd_options', [
          'title' => 'Filter',
          'requireSurvey' => true,
          'showPrintButton' => true
        ])
    </form>

    @if($selectedSurvey)
        <div class="container x_panel" id="section-to-print">
            <div class="text-center">
            <h4>Nilai Unsur Pelayanan</h4>
            <h4>{{ $selectedSkpdData->name }}</h4>
            <h4>{{ $selectedSurveyData->nama }}</h4>
            <h4>Tahun {{ $selectedYear }}</h4>
            </div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center">No</td>
                    <th class="text-center">Unsur Pelayanan</th>
                    <th class="text-center">NRR</th>
                    <th class="text-center">NRR Tertimbang</th>
                    <th class="text-center">IKM UNSUR</th>
                    <th class="text-center">Mutu Pelayanan</td>
                </tr>
                </thead>
                <tbody>
                @php
                    $nrrTertimbang = 0;  
                @endphp
                @foreach($unsurPertanyaan as $key => $value)
                    @php
                    $serviceValue = $value->getAverageServiceValue([$selectedSurvey]);
                    @endphp
                    <tr>
                        <td class="text-center">{{$key + 1}}</td>
                        <td>{{$value->nama}}</td>
                        <td class="text-center">{{ !empty(trim($serviceValue)) ? $serviceValue : '-' }}</td>
                        <td class="text-center">{{ !empty(trim($serviceValue)) ? round($serviceValue * 0.1111, 2) : '-' }}</td>
                        <td class="text-center">{{ !empty(trim($serviceValue)) ? round($serviceValue * 25, 2) : '-' }}</td>
                        <td class="text-center">{{$value->getAverageServiceStatus([$selectedSurvey])}}</td>
                    </tr>
                    @php
                        $nrrTertimbang += round($serviceValue * 0.1111, 2);
                    @endphp
                @endforeach
                    <tr>
                        <td colspan="3">Jumlah NRR Tertimbang</td>
                        <td class="text-center">{{ $nrrTertimbang }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="4">Jumlah IKM Unsur</td>
                        <td class="text-center">{{ $nrrTertimbang * 25 }}</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endif
@endsection



@push('css')
    <style type="text/css">
        @media print {
            body * {
                visibility: hidden;
            }
            #section-to-print, #section-to-print * {
                visibility: visible;
            }
            #section-to-print {
                position: absolute;
                left: 0;
                top: 0;
            }
        }
    </style>
@endpush


<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{{$title}}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Tahun <span class="required">*</span> :
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name='{{$fieldName}}' class="form-control select2" id="tahun">
                            @foreach($availableYears AS $row)
                                @php
                                    $selected = $selectedYear == $row->tahun ? 'selected' : '';
                                @endphp
                                <option value="{{$row->tahun}}" {{$selected}}>{{$row->tahun}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

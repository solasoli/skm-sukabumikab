<link rel="stylesheet" href="{{ asset('/vendors/bootstrap/dist/css/bootstrap.css') }}">
<title>{{ $data['title'] }}</title>
<head>
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
</head>
<style>
    table th, table td {
        color: #FFF;
        border: 0;
    }
    @media print {
        table th, table td {
            color: #333;
        }
    }
</style>
<body>
    <table class="table table-bordered table-stripped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Usia</th>
                <th>JK</th>
                <th>Pendidikan</th>
                <th>Pekerjaan</th>
                <th>E-mail</th>
            </tr>
        </thead>
        <tbody>
            @php
                $no = 1
            @endphp
            @foreach($f_hs AS $data => $value)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $value->nama }}</td>
                    <td>{{ $value->alamat }}</td>
                    <td>{{ $value->usia }}</td>
                    <td>{{ $value->jk }}</td>
                    <td>{{ $value->pendidikan }}</td>
                    <td>{{ $value->pekerjaan }}</td>
                    <td>{{ $value->email }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
<script>
    $(window).on('load', function() {
        window.print();
        setTimeout(function () { window.close(); }, 500)
    })
</script>
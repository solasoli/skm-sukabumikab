<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Survey Kepuasan Masyarakat | Login</title>

        <!-- Bootstrap -->
        <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- NProgress -->
        <link href="{{ asset('vendors/nprogress/nprogress.css') }}" rel="stylesheet">
        <!-- Animate.css -->
        <link href="{{ asset('css/login.css') }}" rel="stylesheet">

        <link href="{{ asset('vendors/animate.css/animate.min.css') }}" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="{{ asset('build/css/custom.min.css') }}" rel="stylesheet">
        <link rel="shortcut icon" href="{{ asset('/img/kotabogor.ico') }}">
    </head>
  <style>
    #text{
        width: 90%;
  padding: 12px 20px;      
  margin: 8px 0;
  box-sizing: border-box;
  text-align: left;
  font-size: 15px;

    }
    
    </style>
    <div class="wrapper fadeInDown">
      <div id="formContent">
        <!-- Tabs Titles -->

        <!-- Icon -->
        <div class="fadeIn first">
          <img src="http://esakip.kotabogor.go.id/images/logo-kotakab.png" id="icon" alt="User Icon" />
         
      </div>

      <!-- Login Form -->
        <body>
        <div>
            <a class="hiddenanchor" id="signup"></a>
            <a class="hiddenanchor" id="signin"></a>


            <div>
                <div>
                    <section class="login_content">
                      @if(Session::has('status'))
                      <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ Session::get('status')}}</strong>
                      </div>
                      @endif
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <h1>Survey Kepuasan Masyarakat</h1>
                            <!-- Logo Disini -->
                            <!-- <img src="{{ asset('img/logo.png') }}" alt="..." style="width:120px; margin:0 auto; margin-bottom:10px"> -->
                            
                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">

                                <div>
                                    <input type="text" id="text" class="fadeIn second" name="username" value="{{ old('username') }}" required autofocus placeholder="Username">

                                    @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div>
                                    <input type="password" id="text" class="fadeIn third" name="password" required placeholder="Password">

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <center><button type="submit" style="border-radius:0px; height:50px; width: 90%; margin-left:5px;" class="btn btn-primary btn-block">
                                            LOGIN
                                        </button>
                                        </center>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                            </label>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </body>  

      <!-- Remind Passowrd -->
      <div id="formFooter">
        © Pemerintah Kota Bogor. &nbsp;&nbsp;&nbsp;&nbsp; 
        
    </div>

</div>
</div>
</html>

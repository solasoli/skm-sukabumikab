<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <style>
        html {
          min-height: 100%;
        }
        body {
            background: linear-gradient(#659c93, #224e71);
            background-repeat: no-repeat;
            background-size: cover;
            margin: 8px !important;
        }

        h2 {
          font-size: 2rem;
        }

        p {
          font-size: 1.5rem;
        }
        .container-fluid {
          padding: keterangan7% 15%;
        }


        .kertas {
          background: #fff;
          box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
          margin: 26px auto 0;
          max-width: 750px;
          min-height: 300px;
          padding: 24px;
          position: relative;
          width: 80%;
        }

        .kertas:before,
        .kertas:after {
          content: "";
          height: 98%;
          position: absolute;
          width: 100%;
          z-index: -1;
        }

        .kertas:before {
          background: #fafafa;
          box-shadow: 0 0 8px rgba(0, 0, 0, 0.2);
          left: -5px;
          top: 4px;
          transform: rotate(-2.5deg);
        }

        .kertas:after {
          background: #f6f6f6;
          box-shadow: 0 0 3px rgba(0, 0, 0, 0.2);
          right: -3px;
          top: 1px;
          transform: rotate(1.4deg);
        }

        .d-none {
            display: none !important;
        }

        .parsley-errors-list {
          list-style-type: none;
          margin:10px 0;
          padding: 0;
          color: #a94442;
        }
    </style>

    <!-- Bootstrap -->
    <link href="{{asset('/vendors/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{ asset('/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('/vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <link href="{{ asset('/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">

    <script src="{{ asset('/vendors/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/vendors/bootstrap/js/carousel.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{asset('/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('/vendors/moment/min/moment-with-locales.js')}}"></script>
    <script src="{{asset('/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <meta charset="utf-8">
    <title>Survey</title>

  </head>


<body>
<form method="POST" data-parsley-validate="" class="form-horizontal form-label-left" id="survey-form">
  {{ csrf_field() }}
<!--Fixed Headline-->
<div class="container-fluid kertas">
    <h2 class="text-center">Survey Kepuasan Masyarakat</h2>
    <!--Start Carousel-->
    <div id="surveyWizzard" class="carousel slide show-when-done" data-ride="carousel"  style="display: none">
        <div class="carousel-inner">

            <div class="item item-first active container-fluid">
                <div class="card">

                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul class="list-unstyled" >
                              @foreach ($errors->all() as $error)
                                  <li>&#8226; {{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
                  <div class="row">

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12">
                        Nama <span class="required">*</span> &nbsp; <span class="pull-right">:</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="nama" required class="form-control" value="{{ !is_null(old('nama')) ? old('nama') : '' }}" data-parsley-error-message="Nama perlu diisi">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12">
                        Alamat <span class="required">*</span> &nbsp; <span class="pull-right">:</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea name="alamat" rows="5" required class="form-control" style="resize:none;" data-parsley-error-message="Alamat harus diisi">{{ !is_null(old('alamat')) ? old('alamat') : '' }}</textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12">
                        Usia <span class="required">*</span> &nbsp; <span class="pull-right">:</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" name="usia" required class="form-control digits_only" value="{{ !is_null(old('usia')) ? old('usia') : '' }}" data-parsley-error-message="Usia harus diisi">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12">
                        Jenis Kelamin <span class="required">*</span> &nbsp; <span class="pull-right">:</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        @php $checked_l = !is_null(old('jk')) && old('jk') == 'L' ? 'checked' : ''; $checked_p = !is_null(old('jk')) && old('jk') == 'P' ? 'checked' : ''; @endphp
                        <input type="radio" name="jk" value="L" {{$checked_l}} class="icheck"  required="required" data-parsley-errors-container="#jk-error-container" data-parsley-error-message="Jenis Kelamin harus diisi"> Laki-laki &nbsp;
                        <input type="radio" name="jk" value="P" {{$checked_p}} class="icheck"> Perempuan
                        <div id='jk-error-container'>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12">
                        Pendidikan Terakhir <span class="required">*</span> &nbsp; <span class="pull-right">:</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" required name="pendidikan" data-parsley-errors-container="#pendidikan-error-container" data-parsley-error-message="Pendidikan harus dipilih">
                            <option value="">- Pilih -</option>
                            @foreach($pendidikan as $row) @php $selected_pendidikan = !is_null(old('pendidikan')) && old('pendidikan') == $row->id ? 'selected' : ''; @endphp
                            <option value="{{$row->id}}" {{$selected_pendidikan}}>{{$row->nama}}</option>
                            @endforeach
                        </select>

                        <div id='pendidikan-error-container'>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12">
                        Pekerjaan <span class="required">*</span> &nbsp; <span class="pull-right">:</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" required name="pekerjaan" data-parsley-errors-container="#pekerjaan-error-container" data-parsley-error-message="Pekerjaan harus dipilih">
                            <option value="">- Pilih -</option>
                            @foreach($pekerjaan as $row) @php $selected_pekerjaan = !is_null(old('pekerjaan')) && old('pekerjaan') == $row->id ? 'selected' : ''; @endphp
                            <option value="{{$row->id}}" {{$selected_pekerjaan}}>{{$row->nama}}</option>
                            @endforeach
                        </select>

                        <div id='pekerjaan-error-container'>
                        </div>
                      </div>
                    </div>

                    <!-- <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12">
                        Email <span class="required">*</span> &nbsp; <span class="pull-right">:</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="email" name="email" required class="form-control" value="{{ !is_null(old('email')) ? old('email') : '' }}" data-parsley-error-message="Email harus diisi / Email tidak valid">
                      </div>
                    </div> -->

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12">
                        No. Tlp <span class="required">*</span> &nbsp; <span class="pull-right">:</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="no_tlp" required class="form-control" value="{{ !is_null(old('no_tlp')) ? old('no_tlp') : '' }}" data-parsley-minlength="5" data-parsley-error-message="No. Tlp Harus diisi">
                      </div>
                    </div>

                      <div class="form-group">
                          <label class="control-label col-md-4 col-sm-4 col-xs-12">
                              Email &nbsp;<span class="pull-right">:</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="email" name="email" class="form-control" value="{{ !is_null(old('email')) ? old('email') : '' }}" data-parsley-minlength="5" data-parsley-error-message="Format email harus benar">
                          </div>
                      </div>
                  </div>
                </div>
            </div>


            @foreach($pertanyaan AS $idx_pertanyaan => $row_pertanyaan)
            @php
            $no = $idx_pertanyaan + 1;  
            @endphp
                <div class="item container-fluid pertanyaan " id="pertanyaan{{ $no }}">
                    @php
                    $no = $idx_pertanyaan + 1;
                    @endphp
                    {{$no}}.{{$row_pertanyaan->pertanyaan}}
                    <br>
                    @if($row_pertanyaan->id_tipe_pertanyaan == 1)
                      @foreach($jawaban AS $idx_jawaban => $row_jawaban)
                        @if($row_pertanyaan->id == $row_jawaban->id_pertanyaan)
                          <div style="display: inline-block; margin-right: 20px">
                            <input type='radio' name='jawaban[{{$row_pertanyaan->guid}}]' value='{{$row_pertanyaan->id}}_{{$row_jawaban->id_jawaban}}' class='icheck'> <span style='vertical-align:bottom'>{{$row_jawaban->label}}</span>
                          </div>
                        @endif
                      @endforeach

                    @else
                      <div style="display: inline-block; margin-right: 0px;font-size: 20px">
                        <input type='hidden' name='jawaban_bintang[{{$row_pertanyaan->guid}}]' value='0' class='jawaban_hide'>
                        @for($i=1;$i<= $max_bobot; $i++)
                          <i class='fa fa-star rate-star' style='color:#d6d6d6' data-pertanyaan-guid="{{$row_pertanyaan->guid}}" data-star="{{$i}}"></i>
                        @endfor
                      </div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
  <div class="row justify-content-between btn-survey-cover show-when-done" style="display: none">
    <div class="col-xs-6">
      <button type="button" id="surveyBackButton" class="btn btn-outline-primary d-none" href="#surveyWizzard" role="button" data-slide="prev">Back</button>
    </div>
    <div class="col-xs-6 text-right">
      <button type="button" id='surveyNextButton' class="btn btn-primary" href="#surveyWizzard" role="button" data-slide="next">Next</button>
      @if(!isset($is_preview) || !$is_preview)
      <button type="submit" id='surveySubmitButton' class="btn btn-success d-none">Submit</button>
      @endif
    </div>
  </div>
    <!--End Carousel-->
</div>
</form>


<br>
<div class="container-fluid kertas" id="keterangan1" style="display: none">
  <div class="row">
    <h3 class="text-center">Keterangan Bintang</h3>
    <div class="col-xs-12">
      <ol>
        <li>Tidak Memuaskan</li>
        <li>Kurang Memuaskan</li>
        <li>Memuaskan</li>
        <li>Sangat Memuaskan</li>
      </ol>
    </div>
  </div>
</div>
<div class="container-fluid kertas" id="keterangan2" style="display: none">
  <div class="row">
    <h3 class="text-center">Keterangan Bintang</h3>
    <div class="col-xs-12">
      <ol>
        <li>Tidak Mudah</li>
        <li>Kurang Mudah</li>
        <li>Mudah</li>
        <li>Sangat Mudah</li>
      </ol>
    </div>
  </div>
</div>
<div class="container-fluid kertas" id="keterangan3" style="display: none">
  <div class="row">
    <h3 class="text-center">Keterangan Bintang</h3>
    <div class="col-xs-12">
      <ol>
        <li>Tidak Cepat</li>
        <li>Kurang Cepat</li>
        <li>Cepat</li>
        <li>Sangat Cepat</li>
      </ol>
    </div>
  </div>
</div>
<div class="container-fluid kertas" id="keterangan4" style="display: none">
  <div class="row">
    <h3 class="text-center">Keterangan Bintang</h3>
    <div class="col-xs-12">
      <ol>
        <li>Tidak Mahal</li>
        <li>Cukup Mahal</li>
        <li>Murah</li>
        <li>Gratis</li>
      </ol>
    </div>
  </div>
</div>
<div class="container-fluid kertas" id="keterangan5" style="display: none">
  <div class="row">
    <h3 class="text-center">Keterangan Bintang</h3>
    <div class="col-xs-12">
      <ol>
        <li>Tidak Sesuai</li>
        <li>Kurang Sesuai</li>
        <li>Sesuai</li>
        <li>Sangat Sesuai</li>
      </ol>
    </div>
  </div>
</div>
<div class="container-fluid kertas" id="keterangan6" style="display: none">
  <div class="row">
    <h3 class="text-center">Keterangan Bintang</h3>
    <div class="col-xs-12">
      <ol>
        <li>Tidak Kompeten</li>
        <li>Kurang Kompeten</li>
        <li>Kompeten</li>
        <li>Sangat Kompeten</li>
      </ol>
    </div>
  </div>
</div>
<div class="container-fluid kertas" id="keterangan7" style="display: none">
  <div class="row">
    <h3 class="text-center">Keterangan Bintang</h3>
    <div class="col-xs-12">
      <ol>
        <li>Tidak Sopan/Ramah</li>
        <li>Kurang Sopan/Ramah</li>
        <li>Sopan/Ramah</li>
        <li>Sangat Sopan/Ramah</li>
      </ol>
    </div>
  </div>
</div>
<div class="container-fluid kertas" id="keterangan8" style="display: none">
  <div class="row">
    <h3 class="text-center">Keterangan Bintang</h3>
    <div class="col-xs-12">
      <ol>
        <li>Buruk</li>
        <li>Cukup</li>
        <li>Baik</li>
        <li>Sangat Baik</li>
      </ol>
    </div>
  </div>
</div>
<div class="container-fluid kertas" id="keterangan9" style="display: none">
  <div class="row">
    <h3 class="text-center">Keterangan Bintang</h3>
    <div class="col-xs-12">
      <ol>
        <li>Tidak Ada</li>
        <li>Ada tapi tidak berfungsi</li>
        <li>Berfungsi kurang maksimal</li>
        <li>Dikelola dengan baik</li>
      </ol>
    </div>
  </div>
</div>
<script src="{{ asset('/vendors/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('/vendors/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('/vendors/parsleyjs/dist/parsley.min.js') }}"></script>

<script type="text/javascript">
  $(function(){

    const srv_chk = localStorage.getItem("{{ $grup->slug }}")

    //if(srv_chk == 1) {
     //$("#surveyWizzard").html(`<br><br><div class='alert alert-danger'>Anda sudah mengisi survey ini</div>`)
     //$(".btn-survey-cover").remove()
    //}
    
    $(".show-when-done").show();

    $('select').select2({
      width:"100%"
    });
    $('input.icheck').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    $(".rate-star").on('click', function(){
      $(this).parent().find($(".jawaban_hide")).val($(this).data("star"));
      var data_guid = $(this).data("pertanyaanGuid");
      for(i = 1;i<={{$max_bobot}}/1;i++){
        if(i <= $(this).data("star")){
          $(".rate-star[data-pertanyaan-guid='" + data_guid +"'][data-star='" + i +"']").css({color:"#ffac28"});
        } else {

          $(".rate-star[data-pertanyaan-guid='" + data_guid +"'][data-star='" + i +"']").css({color:"#d6d6d6"});
        }
      }
    })
  })
</script>
<script>
  if($('card').is(':visible'))
    $('#keterangan1', '#keterangan2', '#keterangan3', '#keterangan4', '#keterangan5', '#keterangan6', '#keterangan7', '#keterangan8', '#keterangan9' ).hide();
  
    var has_clicked_submit = false;
    $("#surveyWizzard").carousel({
      interval: false,
      wrap: false,
      keyboard: false
    });

    function give_last_item_parent() {
        var last_child = $(".carousel-inner .item:last-child");
        if(!last_child.hasClass('item-first')){
            last_child.addClass("last-item");
        }
    }
    give_last_item_parent();

    $('#surveyWizzard').on('slid.bs.carousel', function() {
        console.log("WADADA");
      if($("#pertanyaan1").hasClass("active")) {
       $('#keterangan1').show();
       $('#keterangan2').hide();
       $('#keterangan3').hide();
       $('#keterangan4').hide();
       $('#keterangan5').hide();
       $('#keterangan6').hide();
       $('#keterangan7').hide();
       $('#keterangan8').hide();
       $('#keterangan9').hide(); 
      }
      if($("#pertanyaan2").hasClass("active")) {
       $('#keterangan1').hide();
       $('#keterangan2').show();
       $('#keterangan3').hide();
       $('#keterangan4').hide();
       $('#keterangan5').hide();
       $('#keterangan6').hide();
       $('#keterangan7').hide();
       $('#keterangan8').hide();
       $('#keterangan9').hide(); 
      }
      if($("#pertanyaan3").hasClass("active")) {
       $('#keterangan1').hide();
       $('#keterangan2').hide();
       $('#keterangan3').show();
       $('#keterangan4').hide();
       $('#keterangan5').hide();
       $('#keterangan6').hide();
       $('#keterangan7').hide();
       $('#keterangan8').hide();
       $('#keterangan9').hide(); 
      }
      if($("#pertanyaan4").hasClass("active")) {
       $('#keterangan1').hide();
       $('#keterangan2').hide();
       $('#keterangan3').hide();
       $('#keterangan4').show();
       $('#keterangan5').hide();
       $('#keterangan6').hide();
       $('#keterangan7').hide();
       $('#keterangan8').hide();
       $('#keterangan9').hide(); 
      }
      if($("#pertanyaan5").hasClass("active")) {
       $('#keterangan1').hide();
       $('#keterangan2').hide();
       $('#keterangan3').hide();
       $('#keterangan4').hide();
       $('#keterangan5').show();
       $('#keterangan6').hide();
       $('#keterangan7').hide();
       $('#keterangan8').hide();
       $('#keterangan9').hide(); 
      }
      if($("#pertanyaan6").hasClass("active")) {
       $('#keterangan1').hide();
       $('#keterangan2').hide();
       $('#keterangan3').hide();
       $('#keterangan4').hide();
       $('#keterangan5').hide();
       $('#keterangan6').show();
       $('#keterangan7').hide();
       $('#keterangan8').hide();
       $('#keterangan9').hide(); 
      }
      if($("#pertanyaan7").hasClass("active")) {
       $('#keterangan1').hide();
       $('#keterangan2').hide();
       $('#keterangan3').hide();
       $('#keterangan4').hide();
       $('#keterangan5').hide();
       $('#keterangan6').hide();
       $('#keterangan7').show();
       $('#keterangan8').hide();
       $('#keterangan9').hide(); 
      }
      if($("#pertanyaan8").hasClass("active")) {
       $('#keterangan1').hide();
       $('#keterangan2').hide();
       $('#keterangan3').hide();
       $('#keterangan4').hide();
       $('#keterangan5').hide();
       $('#keterangan6').hide();
       $('#keterangan7').hide();
       $('#keterangan8').show();
       $('#keterangan9').hide(); 
      }
      if($(".item-first").hasClass("active")){
        
        $('#surveyBackButton').addClass('d-none');
        console.log(has_clicked_submit);
        if(has_clicked_submit){

            $("#surveySubmitButton").removeClass('d-none');
        }
      } else {
            $("#surveySubmitButton").addClass('d-none');
        $('#surveyBackButton').removeClass('d-none')
      }

      if($(".last-item").hasClass('active')){
        $("#surveyNextButton").addClass('d-none');
        $("#surveySubmitButton").removeClass('d-none');
      } else {
        $("#surveyNextButton").removeClass('d-none');
      }
      if($('#pertanyaan1').hasClass('active', function() {
        console.log('guguk');
      }));
    });

    
    


  $(function(){
    $('#survey-form').parsley().on('field:validated', function() {
        $('#surveyWizzard').carousel(0);
        has_clicked_submit = true;
        $("#surveySubmitButton").removeClass('d-none');
    });
  });
  
  $('#surveyWizzard').on()

</script>

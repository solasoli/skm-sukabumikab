<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Not Found</title>
</head>
<style>
    .text {
        background-color: #eee;
        position: fixed;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    h1 {
        color: #777;
        text-align: center;
    }
</style>
<body>
    <div class="text">
        <h1>Mohon maaf <br> Survey sudah tidak dipublikasi!</h1>
    </div>
</body>
</html>
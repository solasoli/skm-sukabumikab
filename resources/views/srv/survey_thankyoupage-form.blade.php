<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <style>
      body {
        background: linear-gradient(#659c93, #224e71);
        background-repeat: no-repeat;
        background-size: cover;
        height: 100vh;
        font: 14px sans-serif;
        padding: 20px;
      }

      .kertas {
        background: #fff;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
        margin: 26px auto 0;
        max-width: 750px;
        min-height: 300px;
        padding: 24px;
        position: relative;
        width: 80%;
      }

      .kertas:before,
      .kertas:after {
        content: "";
        height: 98%;
        position: absolute;
        width: 100%;
        z-index: -1;
      }

      .kertas:before {
        background: #fafafa;
        box-shadow: 0 0 8px rgba(0, 0, 0, 0.2);
        left: -5px;
        top: 4px;
        transform: rotate(-2.5deg);
      }

      .kertas:after {
        background: #f6f6f6;
        box-shadow: 0 0 3px rgba(0, 0, 0, 0.2);
        right: -3px;
        top: 1px;
        transform: rotate(1.4deg);
      }

      .data_diri {

      }

      .data_diri td{
        padding:5px;
      }

      .tbl_pertanyaan{

      }

      .tbl_pertanyaan td{
        padding:2px;
      }
    </style>
    <!-- Bootstrap -->
    <link href="{{asset('/vendors/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('/vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <!-- Bootstrap -->
    <script src="{{asset('/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{asset('/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('/vendors/moment/min/moment-with-locales.js')}}"></script>
    <script src="{{asset('/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <meta charset="utf-8">
    <title>Survey</title>
  </head>


<body>
<form method="POST">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content">
          <div class="kertas">
            <h3>Terimakasih, survey anda akan kami review!</h3>
            <hr>
            Terimakasih, telah menyempatkan waktu untuk mengisi survey ini! <br>
            Jika ada kritik dan saran , silahkan isi form di bawah ini
            <br><br>

            <!-- <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">
                Kritik dan Saran <span class="required">*</span> :
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea name="kritik_saran" rows="3" cols="80" class="form-control">{{ !is_null(old('kritik_saran')) ? old('kritik_saran') : '' }}</textarea>
              </div>
            </div> -->

            <table border="0" width="100%">
              <tr>
                <td>
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Kritik dan Saran :</label>
                </td>
              </tr>
              <tr>
                <td>
                  <textarea name="kritik_saran" rows="3" cols="80" class="form-control">{{ !is_null(old('kritik_saran')) ? old('kritik_saran') : '' }}</textarea>
                </td>
              </tr>
              <tr>
                <td height="100px">
                  <a href='{{URL::to('/survey/share/'.$crypt_id)}}' class="btn btn-info" type="button">Lewati</a>&nbsp;
                  <input type="submit" class="btn btn-primary" value="Simpan">
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
<script>
  // register to local storage
  localStorage.setItem("{{ $data->group->slug }}", 1);
</script>

</body>
</html>

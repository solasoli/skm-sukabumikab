@extends('layouts.app')

@section('content')

    <div class="page-title">
      <div class="title_left">
        <h3>Penerbitan</h3>
      </div>
    </div>

    <div class="clearfix"></div>
    @if(Session::has('message'))
      <p class="alert alert-success">{!! Session::get('message') !!}</p>
    @endif <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Filter</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">
              Proses Penerbitan <span class="required">*</span> :
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name='filter' class="form-control select2" id="filter">
                <option value='0'>Belum Diterbitkan</option>
                <option value='1'>Sedang Diterbitkan</option>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <!-- <h2>List</h2> -->
            {{-- <div class="pull-right">
              <a class='btn btn-sm btn-success' href='{{url()->current()}}/add'><i class='fa fa-plus'></i> Tambah</a>
            </div> --}}
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table class="table table-bordered table-striped" id="users-table" style="width:100%">
             <thead>
               <tr>
                 <th>Judul Survey</th>
                 @if(must_show_skpd_form())
                 <th>OPD</th>
                 @endif

                 @if((isset($konfigurasi->default_pertanyaan_tipe_hide) && $konfigurasi->default_pertanyaan_tipe_hide == 0) || !isset($konfigurasi->default_pertanyaan_tipe_hide))
                 <th>Tipe Pertanyaan</th>
                 @endif
                 <th>Tahun</th>
                 <th>Link</th>
                 <th style='width:150px'>Aksi</th>
               </tr>
             </thead>
           </table>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script>

$(function() {
  var table = $('#users-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{url()->current()}}/datatables/' + $("#filter").val(),
      columns: [
        { data: 'judul', name: 'judul' },
        @if(must_show_skpd_form())
        { data: 'skpd', name: 'skpd' },
        @endif

        @if((isset($konfigurasi->default_pertanyaan_tipe_hide) && $konfigurasi->default_pertanyaan_tipe_hide == 0) || !isset($konfigurasi->default_pertanyaan_tipe_hide))
        { data: 'tipe_pertanyaan', name: 'tipe_pertanyaan' },
        @endif
        { data: 'tahun', name: 'tahun' },
        { data: null, orderable: false, render: function ( data, type, row ) {
          if($("#filter").val() == 1) { 
            return "<a href='{{URL::to("/")}}/survey/" + data.slug + "' target='_blank'>{{URL::to("/")}}/survey/" + data.slug +"</a>";
          } else {

            return "<a class='btn btn-primary btn-xs' href='{{URL::to("/")}}/survey/preview/" + data.slug + "' target='_blank'>Lihat Survey</a>";
          }
        }},
        { data: null, orderable: false, render: function ( data, type, row ) {
          var return_button = "";
          @if(can_access_from_url("add"))
          if(data.is_publish == 0){
            return_button += "<a class='btn btn-info btn-xs' href='{{url()->current()}}/publish/" + data.slug + "/1' onclick='return confirm(\"Apakah anda yakin untuk diterbitkan?\")'><i class='fa fa-check'></i> Publish</a>";
          } else {
            return_button += "<a class='btn btn-danger btn-xs' href='{{url()->current()}}/publish/" + data.slug + "/0' onclick='return confirm(\"Apakah anda yakin untuk tidak lagi diterbitkan?\")'><i class='fa fa-close'></i> Publish</a>";
          }
          @endif
          return return_button == "" ? "-" : return_button;
        }},
      ]
  });

  $("#filter").on('change', function(){
    table.ajax.url('{{url()->current()}}/datatables/' + $("#filter").val()).load();
  })

  setTimeout(function() {
    $(".alert-success").hide(1000);
  }, 3000);

});
</script>
@endsection

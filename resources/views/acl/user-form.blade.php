@extends('layouts.app')

@section('content')

<div class="page-title">
  <div class="title_left">
    <h3>User</h3>
  </div>
</div>
<div class="clearfix"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>&#8226; {{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Form {{isset($data) ? "Edit" : "Tambah"}} User</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form class="form-horizontal form-label-left" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">
              Nama User <span class="required">*</span> :
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input name='nama' autocomplete="off" value='{{ !is_null(old('nama')) ? old('nama') : (isset($data->name) ? $data->name : '') }}' required="required" class="form-control col-md-7 col-xs-12" type="text">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">
              Username <span class="required">*</span> :
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input name='username' autocomplete="off" value='{{ !is_null(old('username')) ? old('username') : (isset($data->username) ? $data->username : '') }}' required="required" class="form-control col-md-7 col-xs-12" type="text">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">
              OPD <span class="required">*</span> :
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name='skpd' class="form-control select2">
                @foreach($skpd as $id => $row)

                  @php
                    $selected = !is_null(old('skpd')) ? old('skpd') : (isset($data) ? $data->first()->id_skpd : '');
                    $selected = $selected == $row->id ? "selected" : "";
                  @endphp

                  <option value="{{$row->id}}" {{$selected}}>{{$row->name}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">
              Role <span class="required">*</span> :
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name='role' class="form-control select2">
                @foreach($role as $id => $row)

                  @php
                    $selected = !is_null(old('role')) ? old('role') : (isset($data) ? $data->first()->id_role : '');
                    $selected = $selected == $row->id ? "selected" : "";
                  @endphp

                  <option value="{{$row->id}}" {{$selected}}>{{$row->nama}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">
              Email User <span class="required">*</span> :
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input name='email' autocomplete="off" value='{{ !is_null(old('email')) ? old('email') : (isset($data->email) ? $data->email : '') }}' required="required" class="form-control col-md-7 col-xs-12" type="email">
            </div>
          </div>
          {!! isset($data->id) ? '<h4 class="text-danger" align="center">Jika Password tidak diisi maka password tidak akan dirubah</h4>' : ''!!}
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">
              Password {!! !isset($data->id) ? '<span class="required">*</span>' : ''!!} :
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input name='password' autocomplete="off" {!! !isset($data->id) ? 'required' : ''!!} class="form-control col-md-7 col-xs-12" type="password">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">
              Konfirmasi Password {!! !isset($data->id) ? '<span class="required">*</span>' : ''!!} :
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input name='conf_password' {!! !isset($data->id) ? 'required' : ''!!} class="form-control col-md-7 col-xs-12" type="password">
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <a href='{{url('')}}/acl/role' class="btn btn-primary" type="button">Cancel</a>
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){

});
</script>
@endsection

<?php


namespace App\Services;


use App\SrvPertanyaanUnsur;

class QuestionElementValuesService
{

    private $srvPertanyaanUnsur;
    private $surveyIds = [];
    private $year;
    private $month;

    public function __construct(SrvPertanyaanUnsur $srvPertanyaanUnsur)
    {
        $this->srvPertanyaanUnsur = $srvPertanyaanUnsur;
    }

    public function withSurveyId(int $surveyId)
    {
        $this->surveyIds[] = $surveyId;

        return $this;
    }

    public function withSurveyIds(array $surveyIds)
    {
        $this->surveyIds = array_merge($this->surveyIds, $surveyIds);

        return $this;
    }

    public function withYear(int $yearInFourDigits)
    {
        $this->year = $yearInFourDigits;

        return $this;
    }

    public function withMonth(int $monthInTwoDigits)
    {
        $this->month = $monthInTwoDigits;

        return $this;
    }

    public function getAverageServiceValue()
    {
        return $this->calculateAverageServiceValue();
    }

    public function getAverageServiceStatus()
    {
        $value = $this->calculateAverageServiceValue();

        if($value >= 0 && $value <= 1.75 && $value != null)
            $status = 'Tidak Memuaskan';
        elseIf($value > 1.75 && $value <= 2.50)
            $status = 'Kurang Memuaskan';
        elseIf($value > 2.50 && $value <= 3.25)
            $status = 'Memuaskan';
        elseIf($value > 3.25 && $value <= 4.00)
            $status = 'Sangat Memuaskan';
        elseif($value == null)
            $status = 'Tidak Ada Data';
        else
            $status = '';

        return $status;
    }

    /**
     * Will calculate average service value based from input
     *
     * @return false|float|null
     */
    public function calculateAverageServiceValue()
    {
        /*$bobotTotal = $this->srvPertanyaan()
            ->with('srvJawaban.srvSurveyHasil')
            ->whereHas('srvJawaban', function ($srvJawabanQuery) use ($surveyIds){
                return $srvJawabanQuery->whereHas('srvSurveyHasil', function ($srvSurveyHasilQuery) use ($surveyIds){
                    if(!empty($surveyIds))
                        $srvSurveyHasilQuery->whereIn('id_grup', $surveyIds );
                });
            })
            ->get()->sum(function ($pertanyaan) {
                return $pertanyaan->srvJawaban->sum('bobot');
            });

        $jumlahRespondent = $this->srvPertanyaan()
            ->with(['srvJawaban' => function($srvJawabanQuery) {

                $srvJawabanQuery->withCount('srvSurveyHasil');
            }])
            ->whereHas('srvJawaban', function ($srvJawabanQuery) use ($surveyIds){
                return $srvJawabanQuery->whereHas('srvSurveyHasil', function ($srvSurveyHasilQuery) use ($surveyIds){
                    if(!empty($surveyIds))
                        $srvSurveyHasilQuery->whereIn('id_grup', $surveyIds );
                });
            })
            ->get()->sum(function ($pertanyaan) {
                return $pertanyaan->srvJawaban->sum('srv_survey_hasil_count');
            });*/

        $bobotTotal = $this->srvPertanyaanUnsur->srvPertanyaan()
            ->selectRaw('sum(srv_jawaban.bobot) as bobot_total')
            ->join('srv_survey_hasil_detail', 'srv_survey_hasil_detail.id_pertanyaan', '=', 'srv_pertanyaan.id')
            ->join('srv_jawaban', 'srv_jawaban.id', '=', 'srv_survey_hasil_detail.id_jawaban')
            ->where('srv_survey_hasil_detail.is_deleted', '=', 0);
            //->where('srv_jawaban.is_deleted', '=', 0);

        $jumlahRespondent = $this->srvPertanyaanUnsur->srvPertanyaan()
            ->selectRaw('count(srv_survey_hasil.id) as jumlah_respondent')
            ->join('srv_survey_hasil_detail', 'srv_survey_hasil_detail.id_pertanyaan', '=', 'srv_pertanyaan.id')
            ->join('srv_survey_hasil', 'srv_survey_hasil.id', '=', 'srv_survey_hasil_detail.id_hasil')
            ->where('srv_survey_hasil_detail.is_deleted', '=', 0)
            ->where('srv_survey_hasil.is_deleted', '=', 0)
            ->where('srv_survey_hasil.deleted_at', '=', null)
            ->where('srv_survey_hasil.deleted_by', '=', 0);


        if(!empty($this->surveyIds) || $this->year || $this->month)
            $bobotTotal->join('srv_survey_hasil', 'srv_survey_hasil.id', '=', 'srv_survey_hasil_detail.id_hasil')
                ->where('srv_survey_hasil.is_deleted', '=', 0)
                ->where('srv_survey_hasil.deleted_at', '=', null)
                ->where('srv_survey_hasil.deleted_by', '=', 0);

        if(!empty($this->surveyIds)) {
            $bobotTotal->whereIn('srv_survey_hasil.id_grup', $this->surveyIds);
            $jumlahRespondent->whereIn('srv_survey_hasil.id_grup', $this->surveyIds);
        }

        if($this->year) {
            $bobotTotal->whereRaw('YEAR(srv_survey_hasil.created_at) = ?', [$this->year] );
            $jumlahRespondent->whereRaw('YEAR(srv_survey_hasil.created_at) = ?', [$this->year] );
        }

        if($this->month) {
            $bobotTotal->whereRaw('MONTH(srv_survey_hasil.created_at) = ?', [$this->month] );
            $jumlahRespondent->whereRaw('MONTH(srv_survey_hasil.created_at) = ?', [$this->month] );
        }


        $jumlahRespondent = $jumlahRespondent->first()->jumlah_respondent;
        $bobotTotal = $bobotTotal->first()->bobot_total;


        if($bobotTotal == 0 || $jumlahRespondent == 0)
            return null;

        return round($bobotTotal / $jumlahRespondent, 2);
    }
}

<?php

namespace App\Srv;

use Illuminate\Database\Eloquent\Model;

class Grup extends Model
{
  protected $table = "srv_grup";
  public $timestamps = false;
}

<?php

namespace App\Srv;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
  protected $table = "srv_pertanyaan";
  public $timestamps = false;
}

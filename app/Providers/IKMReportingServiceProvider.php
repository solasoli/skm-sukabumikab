<?php

namespace App\Providers;

use App\Services\IKMReportingService;
use Illuminate\Support\ServiceProvider;

class IKMReportingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('IKMReportingService', function ($app) {
            return new IKMReportingService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

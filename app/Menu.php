<?php

namespace App;

use App\Traits\SoftDelete;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use SoftDelete;
    protected $table = "acl_menu";
    protected $primaryKey = "id";
    public $timestamps = false;
    protected $fillable = [
        'nama',
        'url',
        'slug',
        'id_parent',
        'level'
    ];

    public function childrenMenu()
    {
        return $this->hasMany(Menu::class, 'id_parent');
    }

    public function permissions()
    {
        return $this->hasMany(Permission::class, 'id_menu');
    }

    public function hasMenuWithUrl($routeName)
    {
        $routePath = route($routeName, [], false);
        $hasSameRoute = $this->url == $routePath;

        // Check same url within children menu
        if(!$hasSameRoute && $this->childrenMenu->count() > 0)
            $this->childrenMenu->each(function (Menu $menu) use ($routeName, &$hasSameRoute){
                if($menu->hasMenuWithUrl($routeName)) {
                    $hasSameRoute = true;
                    return false;
                }
            });

        return $hasSameRoute;

    }
}

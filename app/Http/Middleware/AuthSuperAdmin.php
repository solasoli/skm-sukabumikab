<?php

namespace App\Http\Middleware;

use Closure;

class AuthSuperAdmin
{
    /**
     * auth admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isAdmin())
            return $next($request);

        return response()->json([ "message" => "Hanya super admin yang bisa mengakses feature ini"], 403);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Konfigurasi;
use App\Srv\PertanyaanTipe;
use DB;
use App\Menu;

class KonfigurasiController extends Controller
{

    public function tentang()
    {
        $konfigurasi = Konfigurasi::first();
        return View("konfigurasi/tentang-form", [
            'data' => $konfigurasi
        ]);
    }

    public function store_tentang(Request $request)
    {
        $konfigurasi = Konfigurasi::first();
        $konfigurasi->tentang = $request->input('tentang');
        $konfigurasi->save();

        $request->session()->flash('message', "Perubahan berhasil disimpan!");
        return redirect('/konfigurasi/tentang');
    }

    public function skema()
    {
        $konfigurasi = Konfigurasi::first();
        return View("konfigurasi/skema-form", [
            'data' => $konfigurasi
        ]);
    }

    public function store_skema(Request $request)
    {
        $file = $request->file('image');

        if(!is_null($file->getClientOriginalName())){
            //Move Uploaded File
            $destinationPath = public_path().'/img/';
            $file->move($destinationPath,$file->getClientOriginalName());

            $konfigurasi = Konfigurasi::first();
            $konfigurasi->skema = '/img/'.$file->getClientOriginalName();
            $konfigurasi->save();

        }

        $request->session()->flash('message', "Perubahan berhasil disimpan!");
        return redirect('/konfigurasi/skema');
    }


    public function default_jawaban()
    {

        $konfigurasi = Konfigurasi::first();
        $konfigurasi->default_jawaban = json_decode($konfigurasi->default_jawaban);
        return View("konfigurasi/default_jawaban-form", [
            'data' => $konfigurasi
        ]);
    }


    public function get_default_jawaban($guid)
    {
        $pertanyaan_tipe = PertanyaanTipe::where('guid',$guid)->get()->first();
        $pertanyaan_tipe = isset($pertanyaan_tipe->id) ? $pertanyaan_tipe->id : 0;

        //find if pertanyaan tipe is exist
        $pertanyaan_tipe = PertanyaanTipe::findOrFail($pertanyaan_tipe);
        $tipe = $pertanyaan_tipe->id == 1 ? 'opsional' : 'bintang';

        //find the default jawaban
        $konfigurasi = Konfigurasi::first();
        $konfigurasi->default_jawaban = json_decode($konfigurasi->default_jawaban);

        $default_jawaban = isset($konfigurasi->default_jawaban->{$tipe}) ? $konfigurasi->default_jawaban->{$tipe} : [];


        return response()->json(['data' => $default_jawaban, 'msg' => 'Success to find default answer!']);
        //salah saya ieu mas nis , kabaca na string , kedah kieu meh janten json hmmmmmm okay
        // return json_encode($default_jawaban);
    }


    public function store_default_jawaban(Request $request)
    {
        $arr = [];
        foreach ($request->input('pilihan') as $idx => $row) {
            $arr[$row] = [
                'label' => $request->input('jawaban')[$idx],
                'bobot' => $request->input('bobot')[$idx]
            ];
        }


        $konfigurasi = Konfigurasi::first();
        $konfigurasi->default_jawaban = json_encode($arr);
        $konfigurasi->save();

        $request->session()->flash('message', "Perubahan berhasil disimpan!");
        return redirect('/konfigurasi/default_jawaban');
    }

    function sync_menu(){
      $menu = [
          "Tentang SKM" => "/dashboard/tentang",
          "Skema SKM" => "/dashboard/skema",
          "Survey" => [
            "Buat Pertanyaan" => "/srv/pertanyaan",
            "Buat Survey" => "/srv/grup_pertanyaan",
            "Penerbitan" => "/srv/penerbitan"
          ],
          "IKM" => [
            "Per Tahun" => "/laporan/srv_tahunan",
            "Per Bulan" => "/laporan/srv_bulanan",
            "Per OPD" => "/laporan/ikm",
            "IKM Tingkat Kota" => "/laporan/ikm_tingkat_kota",
            
          ],
          "Konfigurasi" => [
            "Tentang SKM" => "/konfigurasi/tentang",
            "Skema SKM" => "/konfigurasi/skema",
            
          ],
          "System" => [
            "Role" => "/acl/role",
            "Menu" => "/acl/menu",
            "Permission" => "/acl/permission",
            "User" => "/acl/user"
          ]
        ];

        echo "<pre>";

        DB::statement("TRUNCATE TABLE acl_menu");
        foreach($menu as $idx => $row){
          $parent_menu_1_link = is_array($row) ? '#' :  $row;
          $parent_menu_1_slug = is_array($row) ? strtolower($idx) : str_replace("/", "_", substr(strtolower($row), 1, 500));
          $parent_menu_1 = new Menu;
          $parent_menu_1->nama = $idx;
          $parent_menu_1->url = $parent_menu_1_link;
          $parent_menu_1->slug = $parent_menu_1_slug;
          $parent_menu_1->level = 1;
          $parent_menu_1->save();

          if(is_array($row)){
            
            $parent_menu_1->have_child = 1;
            $parent_menu_1->save();

            foreach($row as $id => $rw){
              if(is_array($rw)){
                $parent_menu_2 = new Menu;
                $parent_menu_2->nama = $id;
                $parent_menu_2->url = '#';
                $parent_menu_2->slug =  strtolower($id);
                $parent_menu_2->have_child = 1;
                $parent_menu_2->level = 2;
                $parent_menu_2->id_parent = $parent_menu_1->id;
                $parent_menu_2->save();

                foreach($rw as $i => $r){

                  $parent_menu_3 = new Menu;
                  $parent_menu_3->nama = $i;
                  $parent_menu_3->url = $r;
                  $parent_menu_3->slug = str_replace("/", "_", substr(strtolower($r), 1, 500));
                  $parent_menu_3->id_parent = $parent_menu_2->id;
                  $parent_menu_3->level = 3;
                  $parent_menu_3->save();
                  
                }
              }
              else 
              {
                $parent_menu_2 = new Menu;
                $parent_menu_2->nama = $id;
                $parent_menu_2->url = $rw;
                $parent_menu_2->slug = str_replace("/", "_", substr(strtolower($rw), 1,500));
                $parent_menu_2->id_parent = $parent_menu_1->id;
                $parent_menu_2->level = 2;
                $parent_menu_2->save();
              }
            }
          } 
          else 
          {
            /*
            $parent_menu_1 = new Menu;
            $parent_menu_1->nama = $idx;
            $parent_menu_1->url = $row;
            $parent_menu_1->slug = strtolower($idx);
            $parent_menu_1->level = 1;
            $parent_menu_1->save();*/
          }
          echo "<br>";
        }
        echo "</pre>";

        dd($menu);
    }

}

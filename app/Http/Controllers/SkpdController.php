<?php

namespace App\Http\Controllers;

use App\SKPD;
use Illuminate\Http\Request;

class SkpdController extends Controller
{
    public function skpdWithoutSurvey($year)
    {
        $skpds = SKPD::whereDoesntHave('srvGroup', function ($srvGroupQuery) use($year) {
            return $srvGroupQuery->where('tahun', $year);
        });

        return datatables($skpds)->toJson();
    }
}

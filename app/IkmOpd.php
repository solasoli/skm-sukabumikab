<?php


namespace App;


class IkmOpd
{
    private $srvGroup;
    private $skpd;
    private $jumlahBobot;
    private $bobotMaximal;
    public $ikm;

    public function __construct(SKPD $skpd, $jumlahBobot, $bobotMaximal, $ikm, SrvGroup $srvGroup = null)
    {
        $this->skpd = $skpd;
        $this->srvGroup = $srvGroup;
        $this->jumlahBobot = $jumlahBobot;
        $this->bobotMaximal = $bobotMaximal;
        $this->ikm = $ikm;
    }

    public function getSrvGroup()
    {
        return $this->srvGroup;
    }

    public function getSkpd()
    {
        return $this->skpd;
    }

    public function getJumlahBobot()
    {
        return $this->jumlahBobot;
    }

    public function getBobotMaximal()
    {
        return $this->bobotMaximal;
    }

    public function getIkm()
    {
        return $this->ikm;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    const PENDIDIKAN_SD_ID = 1;
    const PENDIDIKAN_SMP_ID = 2;
    const PENDIDIKAN_SMA_ID = 3;
    const PENDIDIKAN_D1_ID = 4;
    const PENDIDIKAN_D2_ID = 5;
    const PENDIDIKAN_D3_ID = 6;
    const PENDIDIKAN_S1_ID = 7;
    const PENDIDIKAN_S2_ID = 8;
    const PENDIDIKAN_S3_ID = 9;

    const PENDIDIKAN_NAME_FROM_ID = [
        self::PENDIDIKAN_SD_ID => 'SD',
        self::PENDIDIKAN_SMP_ID => 'SMP',
        self::PENDIDIKAN_SMA_ID => 'SMA/K',
        self::PENDIDIKAN_D1_ID => 'D1',
        self::PENDIDIKAN_D2_ID => 'D2',
        self::PENDIDIKAN_D3_ID => 'D3',
        self::PENDIDIKAN_S1_ID => 'S1',
        self::PENDIDIKAN_S2_ID => 'S2',
        self::PENDIDIKAN_S3_ID => 'S3',
    ];

    protected $table = "mst_pendidikan";
    protected $primaryKey = "id";
    public $timestamps = false;
}
